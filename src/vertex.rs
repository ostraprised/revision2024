use std::mem::size_of;

use vulkanalia::prelude::v1_0::*; // More vulkan imports

use crate::types::{Vec2, Vec3, Vec4};

// Vertex struct for defining them outside of the vertex shader code
#[repr(C)]
#[derive(Copy, Clone, Debug)]
pub struct Vertex {
    pos: Vec3,
    color: Vec4,
    tex_coord: Vec2,
}

impl Vertex {
    pub fn new(pos: Vec3, color: Vec4, tex_coord: Vec2) -> Self {
        Self {
            pos,
            color,
            tex_coord,
        }
    }
    pub fn binding_description() -> vk::VertexInputBindingDescription {
        /*
         * A vertex binding describes at which rate to load data from memory
         * throughout the vertices. It specifies the number of bytes between
         * data entries and whether to move to the next data entry after each
         * vertex or after each instance.
         */
        vk::VertexInputBindingDescription::builder()
            .binding(0) // Binding index, only one binding for now
            .stride(size_of::<Vertex>() as u32) // Number of bytes between entries
            .input_rate(vk::VertexInputRate::VERTEX) // Render per vertex, not per instance
            .build()
    }
    pub fn attribute_descriptions() -> [vk::VertexInputAttributeDescription; 3] {
        /*
         * Describe how to extract position and color data from vertex data chunks
         */

        //f32 – vk::Format::R32_SFLOAT
        //Vec2 – vk::Format::R32G32_SFLOAT
        //Vec3 – vk::Format::R32G32B32_SFLOAT
        //Vec4 – vk::Format::R32G32B32A32_SFLOAT
        let pos = vk::VertexInputAttributeDescription::builder()
            .binding(0)
            .location(0) // Input directive in vertex shader
            .format(vk::Format::R32G32B32_SFLOAT)
            .offset(0)
            .build();
        let color = vk::VertexInputAttributeDescription::builder()
            .binding(0)
            .location(1)
            .format(vk::Format::R32G32B32A32_SFLOAT)
            .offset(size_of::<Vec3>() as u32)
            .build();
        let tex_coord = vk::VertexInputAttributeDescription::builder()
            .binding(0)
            .location(2)
            .format(vk::Format::R32G32_SFLOAT)
            .offset((size_of::<Vec3>() + size_of::<Vec4>()) as u32)
            .build();
        [pos, color, tex_coord]
    }
}
