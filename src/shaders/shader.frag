#version 450 core

layout(location = 0) in vec4 fragColor;
layout(location = 1) in vec2 fragTexCoord;

layout(binding = 1) uniform sampler2D texSampler;
layout(push_constant) uniform PushConstants {
    layout(offset = 64) float opacity;
} pcs;

layout(location = 0) out vec4 outColor;

void main() {
    vec4 frag_coord = gl_FragCoord;
    outColor = vec4(
        fragTexCoord.x,
        sin(fragTexCoord.y),
        0.0,
        pcs.opacity
    );

    // If we want to apply a texture
    //outColor = vec4(texture(texSampler, fragTexCoord).rgb,pcs.opacity);

    // Just static color for debugging
    //outColor = vec4(0.1,0.5,0.1,pcs.opacity);
}
