#version 450 core

layout(location = 0) in vec4 fragColor;
layout(location = 1) in float time_out;

// Uniforms!
// UBOs in frag shaders really scare the validation, use exports from the vertex shaders
// or push constants instead.
//layout(binding = 0) uniform UniformBufferObject {
//    mat4 model;
//    mat4 view;
//    mat4 proj;
//    vec4 col_mult;
//    float time;
//} ubo;

// Uniforms!
layout(binding = 0) uniform UniformBufferObject {
    mat4 view;
    mat4 proj;
    vec2 width_height;
} ubo;

layout(location = 0) out vec4 outColor;

void main() {
    vec4 frag_coord =  gl_FragCoord;
    //if ( sin(frag_coord.x/40 + cos(frag_coord.y/20) + time_out) < 0) {
    //    //outColor = vec4(0.1, 0.9 - cos(time_out*2) * 0.5, 0.1, 1.0);
    //} else {
    //    outColor = vec4(
    //        fragColor.x,
    //        fragColor.y,
    //        fragColor.z,
    //        fragColor.w
    //    );
    //}
    vec2 p = frag_coord.xy / ubo.width_height;
    outColor = vec4(p.x,p.y,0.1,0.1);
}
