#version 450 core

/// Vertex attributes, specified per vertex in the buffer
layout(location = 0) in vec3 inPosition;  // Some of these can use multiple slots, beware
layout(location = 1) in vec4 inColor;
layout(location = 2) in vec2 inTexCoord;

// Uniforms!
layout(binding = 0) uniform UniformBufferObject {
    mat4 view;
    mat4 proj;
} ubo;

layout(push_constant) uniform PushConstants {
    mat4 model;
} pcs;

layout(location = 0) out vec4 fragColor;
layout(location = 1) out vec2 fragTexCoord;

void main() {
    gl_Position = ubo.proj * ubo.view * pcs.model * vec4(inPosition, 1.0);
    fragColor = inColor;
    fragTexCoord = inTexCoord;
}
