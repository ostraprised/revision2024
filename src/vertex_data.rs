use lazy_static::lazy_static;

use cgmath::{vec2, vec3, vec4};

use crate::vertex::Vertex;

//================================================
// Vertex data
//================================================

// Define our vertices
lazy_static! {
    pub static ref VERTICES: Vec<Vertex> = vec![
        // Cube base vertices
        Vertex::new(
            vec3(-1.5, -1.5, -1.5),
            vec4(1.0, 1.0, 1.0, 1.0),
            vec2(1.0, 0.0),
        ),
        Vertex::new(
            vec3(1.5, -1.5, -1.5),
            vec4(1.0, 1.0, 1.0, 1.0),
            vec2(0.0, 0.0),
        ),
        Vertex::new(
            vec3(1.5, 1.5, -1.5),
            vec4(1.0, 1.0, 1.0, 1.0),
            vec2(0.0, 1.0),
        ),
        Vertex::new(
            vec3(-1.5, 1.5, -1.5),
            vec4(1.0, 1.0, 1.0, 1.0),
            vec2(1.0, 1.0),
        ),
        // Cube top vertices
        Vertex::new(
            vec3(-1.5, -1.5, 1.5),
            vec4(1.0, 1.0, 1.0, 1.0),
            vec2(1.0, 0.0),
        ),
        Vertex::new(
            vec3(1.5, -1.5, 1.5),
            vec4(1.0, 1.0, 1.0, 1.0),
            vec2(0.0, 0.0),
        ),
        Vertex::new(
            vec3(1.5, 1.5, 1.5),
            vec4(1.0, 1.0, 1.0, 1.0),
            vec2(0.0, 1.0),
        ),
        Vertex::new(
            vec3(-1.5, 1.5, 1.5),
            vec4(1.0, 1.0, 1.0, 1.0),
            vec2(1.0, 1.0)
        ),
        // Background vertices
        Vertex::new(
            vec3(-1.0, -1.0, -1.0),
            vec4(1.0, 1.0, 1.0, 1.0),
            vec2(0.0, 0.0),
        ),
        Vertex::new(
            vec3(1.0, -1.0, -1.0),
            vec4(1.0, 1.0, 1.0, 1.0),
            vec2(0.0, 0.0),
        ),
        Vertex::new(
            vec3(1.0, 1.0, -1.0),
            vec4(1.0, 1.0, 1.0, 1.0),
            vec2(0.0, 0.0),
        ),
        Vertex::new(
            vec3(-1.0, 1.0, -1.0),
            vec4(1.0, 1.0, 1.0, 1.0),
            vec2(0.0, 0.0),
        ),
    ];
}

/*      7-------- 6
 *     /         /|
 *    4 --------5 |
 *    |         | |
 *    |         | |
 *    |  3      | 2
 *    |         |/
 *    0---------1
 */

/*
 *
 *      3---------2
 *     /         /
 *    0---------1
 */

// Index buffer for deduplicating overlapping vertices when triangles share edges
// Use u32 if more than 65,536 vertices are needed
pub const INDICES: &[u16] = &[
    // Background
    8, 9, 10, 10, 11, 8, // Plane
    // Cube
    //0, 1, 2, 2, 3, 0, // Base, comment out to hide back of cube when looking from above
    4, 5, 6, 6, 7, 4, // Top
    5, 4, 0, 0, 1, 5, // Front
    1, 2, 5, 2, 6, 5, // Right
    2, 3, 6, 6, 3, 7, // Back
    7, 3, 0, 4, 7, 0, // Left
];
