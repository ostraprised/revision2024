# Rebuild shaders

all: shaders
	cargo build

shaders: vert.spv frag.spv background_vert.spv background_frag.spv

background_vert.spv:
	/usr/bin/glslc src/shaders/background.vert -o src/shaders/background_vert.spv
background_frag.spv:
	/usr/bin/glslc src/shaders/background.frag -o src/shaders/background_frag.spv

vert.spv:
	/usr/bin/glslc src/shaders/shader.vert -o src/shaders/vert.spv
frag.spv:
	/usr/bin/glslc src/shaders/shader.frag -o src/shaders/frag.spv

